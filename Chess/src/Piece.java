//
public class Piece {

	private TypeOfPiece type;
	private Color color;

	public Piece(TypeOfPiece type, Color color) {
		this.type = type;
		this.color = color;
	}

	@Override
	public String toString() {
		String s;
		if (color == Color.BLACK) {
			s = "BLACK";
		} else {
			s = "WHITE";
		}
		s += "-";
		switch (type) {
		case PAWN:
			s = s + "PAWN";
			break;
		case TOWER:
			s = s + "TOWER";
			break;
		case KNIGHT:
			s = s + "KNIGHT";
			break;
		case BISHOP:
			s = s + "BISHOP";
			break;
		case QUEEN:
			s = s + "QUEEN";
			break;
		case KING:
			s = s + "KING";
			break;
		}
		return s;
	}

	public TypeOfPiece getType() {
		return type;
	}

	public Color getColor() {
		return color;
	}

	public String getLetter() {
		String s = "";
		switch (type) {
		case PAWN:
			s = "p";
			break;
		case TOWER:
			s = "t";
			break;
		case KNIGHT:
			s = "k";
			break;
		case BISHOP:
			s = "b";
			break;
		case QUEEN:
			s = "q";
			break;
		case KING:
			s = "x";
			break;
		}
		if (color == Color.BLACK) {
			s = s.toUpperCase();
		}
		return s;
	}
}
