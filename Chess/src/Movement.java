
public class Movement {

	Piece p;
	public char originColumn;
	public char destinationColumn;
	public int originRow;
	public int destinationRow;

	public Movement(Piece p, char originColumn, int originRow, char destinationColumn, int destinationRow) {
		this.p = p;
		this.originColumn = originColumn;
		this.originRow = originRow;
		this.destinationColumn = destinationColumn;
		this.destinationRow = destinationRow;
	}

	public Piece getP() {
		return p;
	}

	public char getOriginColumn() {
		return originColumn;
	}

	public char getDestinationColumn() {
		return destinationColumn;
	}

	public int getOriginRow() {
		return originRow;
	}

	public int getDestinationRow() {
		return destinationRow;
	}

	public String toString() {
		return p.toString() + " from " + originColumn + originRow + " to " + destinationColumn + destinationRow;
	}
}
