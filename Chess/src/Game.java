
public class Game {

	private Movement[] moves;
	private Board b;
	private int position;

	public Game() {
		b = new Board();
		moves = new Movement[100];
		position = 0;
	}

	public void movePiece(Movement mov) {
		moves[position] = mov;
		b.movePiece(mov);
		position++;
	}

	@Override
	public String toString() {
		String s = "";
		s += b.toString();
		s += "\n";
		for (int i = 0; i <= position; i++) {
			s += "Move" + i + ": " + moves[i] + "\n";
		}
		return s;
	}

	public Movement[] getMoves() {
		return moves;
	}

	public Board getB() {
		return b;
	}

	public int getPosition() {
		return position;
	}
	

	public int getMovesPosibles() {
		return moves.length;
	}

}
