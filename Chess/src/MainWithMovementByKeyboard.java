import java.util.Scanner;

public class MainWithMovementByKeyboard {

	public static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		Game one = new Game();

		while (one.getPosition() < one.getMovesPosibles()) {
			Movement prove = makeMoves(input, one);
			one.movePiece(prove);
			System.out.println(one.getB().toString());
		}
	}

	private static Movement makeMoves(Scanner input, Game one) {
		System.out.print("What is her origin col of the piece you want to move?");
		char origCol = input.next().toUpperCase().charAt(0);
		System.out.print("And row?");
		int origRow = input.nextInt();
		System.out.print("And what is her final col?");
		char destCol = 'a';
		destCol = input.next().toUpperCase().charAt(0);
		System.out.print("And row?");
		int destRow = input.nextInt();

		return new Movement(one.getB().getPieceAt(origCol, origRow), origCol, origRow, destCol, destRow);
	}

}
