//
public class MainChess {

	public static void main(String[] args) {
		/*
		 * Piece p = new Piece(TypeOfPiece.BISHOP, Color.BLACK); Piece p2 = new
		 * Piece(TypeOfPiece.PAWN, Color.WHITE); System.out.println(p.toString());
		 * 
		 * Board b = new Board(); System.out.println(b.toString()); Movement m = new
		 * Movement(b.getPieceAt('E', 2), 'E', 2, 'E', 4); b.movePiece(m);
		 * System.out.println(b.toString());
		 */

		Game one = new Game();
		Movement m1 = new Movement(one.getB().getPieceAt('E', 2), 'E', 2, 'E', 4);
		Movement m2 = new Movement(one.getB().getPieceAt('E', 7), 'E', 7, 'E', 5);
		Movement m3 = new Movement(one.getB().getPieceAt('D', 2), 'D', 2, 'D', 3);
		Movement m4 = new Movement(one.getB().getPieceAt('F', 8), 'F', 8, 'B', 4);
		one.movePiece(m1);
		one.movePiece(m2);
		one.movePiece(m3);
		one.movePiece(m4);
		System.out.println(one.toString());
	}

}
